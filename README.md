Use the tool: https://mythiclogo.netlify.com

# Mythic Table Logo Generator

This tool generates a copy of the Mythic Table logo in either an SVG or PNG format. Set the size, foreground and background colors, variant and file type, then download your image. The tool figures out whether the color you've picked requires a dark or light background and adjusts the app's background accordingly.